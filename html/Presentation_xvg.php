<?php

session_start();

if(!isset($_SESSION["ID"])){
session_unset();
session_destroy();

header('Location: index.html'); // Renvoie Vers la page index.php

exit(); // Fin de l'éxecution du PHP

}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style_xvg.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue_grey-pink.min.css">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="../lib/particles.js-master/particles.min.js"></script>
    <script src="../lib/particles.js-master/app.js"></script>
    <title>Presentation : Verge (xvg)</title>
</head>

<body>
    <div id="particles-js">
        <!-- Always shows a header, even in smaller screens. -->
        <div class="mdl-layout mdl-layout mdl-js-layout mdl-layout--fixed-header bg">
            <header class="mdl-layout__header">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <span class="mdl-layout-title">Presentation : Verge (xvg)</span>
                    <!-- Add spacer, to align navigation to the right -->
                    <div class="mdl-layout-spacer"></div>
                    <!-- Navigation. We hide it in small screens. -->
                    <nav class="mdl-navigation mdl-layout--large-screen-only">
                        <a class="mdl-navigation__link" href="./ecommerce/acceuil.html">E-Commerce</a>
                    </nav>
                </div>
            </header>



            <div class="mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50" aria-hidden="true" style="display: flex">

                <header class="demo-drawer-header">
                    <span class="mdl-chip__contact_contact mdl-color--teal mdl-color-text--white">Admin</span>
                    <div class="demo-avatar-dropdown">
                        <span>Administrteur</span>
                        <div class="mdl-layout-spacer"></div>
                    </div>
                </header>

                <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
                    <a class="mdl-navigation__link" href="">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Page d'accueil</a>
                    <a class="mdl-navigation__link" href="./article.php">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">description</i>Article</a>
                    <a class="mdl-navigation__link" href="./lecteur_csv.php">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">description</i>Lecteur de CSV</a>
                    <a class="mdl-navigation__link" href="./phishing_Cdiscount.html">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">priority_high</i>Phishing Cdiscount</a>
                    <a class="mdl-navigation__link" href="./ecommerce/acceuil.html">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">shopping_basket</i>E-commerce</a>
                    <div class="mdl-layout-spacer"></div>
                    <a class="mdl-navigation__link" href="../php/deconnection.php">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">power_settings_new</i>Déconnection</a>
                </nav>
            </div>

            <main class="mdl-layout__content" style="height: 90%;">
                <div class="page-content">
                    <!-- Your content goes here -->
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--6-col" id="center">

                            <div class="article_xvg mdl-card mdl-shadow--2dp">
                                <div class="article_titre mdl-card__title">
                                    <h1 class="mdl-card__title-text">La Crypto-momnaie Verge (XVG)</h1>
                                </div>
                                <div class="mdl-card__supporting-text">
                                    <p>Verge (XVG) est une Crypto-monnaie décentralisée et open-source créée en octobre 2014
                                        et issue d'un fork du Dogecoin. Sa principale caractéristique est d'être anonyme
                                        et intraçable.</p>
                                    <p>Issue d'un fork du Dogecoin et d'abord créée sous le nom de DogecoinDark, la Crypto-monnaie
                                        a été re-nommée Verge en février 2016.
                                        <br> En utilisant des réseaux multiples axés sur l'anonymat, tels que Tor et i2p, les
                                        adresses IP des utilisateurs sont cachés et les transactions sont complètement intraçables.
                                        <br> En plus d'êtres intraçables, les transactions de Verge sont êxtrement rapides. La
                                        technologie "Simple Payment Verification" (SPV) permet un temps moyen de confirmation
                                        de 5 secondes pour les transactions de XVG.Verge utilise également un support d'exploration
                                        de type mutli-algorithme (Scrypt, x17, groestl, blake2s, lyra2rev2) pour améliorer
                                        la sécurité et fournir une répartition égale de monnaie aux mineurs.
                                    </p>
                                    <p>Sources :
                                        <a href="http://www.bitcoin-france.net">bitcoin-france.net</a>
                                    </p>
                                </div>
                                <div class="mdl-card__menu">
                                    <button id="tt4" class="mdl-button mdl-js-button mdl-button--icon">
                                        <i class="material-icons">share</i>
                                    </button>
                                    <div class="mdl-tooltip" for="tt4">
                                        Partager la page
                                        <br>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="mdl-cell mdl-cell--4-col" id="center">

                            <h5>
                                <font color="white">Cours actuel du XVG (selon le site
                                    <a href="https://coinmarketcap.com/">coinmarketcap.com</a>) : </font>
                            </h5>

                            <script type="text/javascript" src="https://files.coinmarketcap.com/static/widget/currency.js"></script>
                            <div class="coinmarketcap-currency-widget scriptCoinMarket" data-currencyid="693" data-base="EUR" data-secondary="BTC" data-ticker="true"
                                data-rank="true" data-marketcap="true" data-volume="true" data-stats="EUR" data-statsticker="false"></div>

                        </div>

                    </div>
                </div>
            </main>
            <footer class="mdl-mini-footer">
                <div class="mdl-mini-footer__left-section">
                    <div class="mdl-logo">Site crée par</div>
                    <ul class="mdl-mini-footer__link-list">
                        <li>
                            <a href="">Florian287001</a>
                        </li>
                    </ul>
                </div>
                <div class="mdl-mini-footer__right-section">
                    <div class="mdl-logo">FrameWork utilisé :</div>
                    <ul class="mdl-mini-footer__link-list">
                        <li>
                            <a href="https://getmdl.io/" target="_blank">Material Design Lite</a>
                        </li>
                    </ul>
                </div>
            </footer>
        </div>
    </div>

    <script>
        particlesJS.load('particles-js', 'particles.json',
            function () {
                console.log('chargement de particles.json...')
            })
    </script>

</body>


</html>