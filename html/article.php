<?php

session_start();

if(!isset($_SESSION["ID"])){
session_unset();
session_destroy();

header('Location: index.html'); // Renvoie Vers la page index.php

exit(); // Fin de l'éxecution du PHP

}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style_xvg.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue_grey-pink.min.css">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="../lib/particles.js-master/particles.min.js"></script>
    <script src="../lib/particles.js-master/app.js"></script>
    <title>Article : SpaceX réussit son lancement mais rate la récupération de la coiffe du Falcon 9</title>
</head>

<body>
    <div id="particles-js">
        <!-- Always shows a header, even in smaller screens. -->
        <div class="mdl-layout mdl-layout mdl-js-layout mdl-layout--fixed-header bg">
            <header class="mdl-layout__header">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <span class="mdl-layout-title">Article : SpaceX réussit son lancement mais rate la récupération de la coiffe du Falcon 9</span>
                    <!-- Add spacer, to align navigation to the right -->
                    <div class="mdl-layout-spacer"></div>
                    <!-- Navigation. We hide it in small screens. -->
                    <nav class="mdl-navigation mdl-layout--large-screen-only">
                        <a class="mdl-navigation__link" href="./ecommerce/acceuil.html">E-Commerce</a>
                    </nav>
                </div>
            </header>



            <div class="mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50" aria-hidden="true" style="display: flex">

                <header class="demo-drawer-header">
                    <span class="mdl-chip__contact_contact mdl-color--teal mdl-color-text--white">Admin</span>
                    <div class="demo-avatar-dropdown">
                        <span>Administrteur</span>
                        <div class="mdl-layout-spacer"></div>
                    </div>
                </header>

                <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
                    <a class="mdl-navigation__link" href="">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Page d'accueil</a>
                    <a class="mdl-navigation__link" href="./Presentation_xvg.php">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">description</i>Présentation XVG</a>
                    <a class="mdl-navigation__link" href="./lecteur_csv.php">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">description</i>Lecteur de CSV</a>
                    <a class="mdl-navigation__link" href="./phishing_Cdiscount.html">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">priority_high</i>Phishing Cdiscount</a>
                    <a class="mdl-navigation__link" href="./ecommerce/acceuil.html">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">shopping_basket</i>E-commerce</a>
                    <div class="mdl-layout-spacer"></div>
                    <a class="mdl-navigation__link" href="../php/deconnection.php">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">power_settings_new</i>Déconnection</a>
                </nav>
            </div>

            <main class="mdl-layout__content" style="height: 90%;">
                <div class="page-content">
                    <!-- Your content goes here -->
                    <div class="mdl-grid">
                        <div class="mdl-cell mdl-cell--2-col mdl-cell--hide-tablet mdl-cell--hide-phone"></div>
                        <div class="article mdl-color--white mdl-shadow--4dp content mdl-color-text--grey-800 mdl-cell mdl-cell--8-col">
                            <div class="demo-crumbs mdl-color-text--grey-500">
                                Acceuil > Article
                            </div>
                            <h3>SpaceX réussit son lancement mais rate la récupération de la coiffe du Falcon 9</h3>
                            <iframe width="854" height="480" src="https://www.youtube.com/embed/mp0TW8vkCLg" frameborder="0" allow="autoplay; encrypted-media"
                                allowfullscreen></iframe>
                            <p>
                                Le décollage de la mission Iridium Next 41-50 a eu lieu vendredi 30 mars. Un lancement que l'on pouvait suivre en direct.
                            </p>
                            <p>
                                <strong>Mise à jour &#8211;</strong> La mission Iridium Next 41-50 s&rsquo;est bien passée. Tous
                                les satellites ont pu être libérés et placés en orbite basse,
                                <a href="https://twitter.com/SpaceX/status/979742157580849152" target="_blank" rel="">a annoncé</a> SpaceX. Par contre, l&rsquo;entreprise américaine n&rsquo;a pas réussi dans
                                sa tentative de récupération de la coiffe. C&rsquo;est ce qu&rsquo;a déclaré Elon Musk, le
                                PDG du groupe, un peu avant 19 heures. Il est prévu de réaliser des tests complémentaires
                                dans les semaines à venir pour résoudre ce problème de parachute.
                            </p>

                            <blockquote class="twitter-tweet" data-lang="fr">
                                <p lang="en" dir="ltr">GPS guided parafoil twisted, so fairing impacted water at high speed. Air wake from fairing
                                    messing w parafoil steering. Doing helo drop tests in next few weeks to solve.</p>&mdash;
                                Elon Musk (@elonmusk)
                                <a href="https://twitter.com/elonmusk/status/979764513233715200?ref_src=twsrc%5Etfw">30 mars 2018</a>
                            </blockquote>
                            <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>

                            <div class="mdl-grid">
                                <div class="mdl-cell mdl-cell--4-col">
                                    <div class="demo-card-image mdl-card un mdl-shadow--2dp">
                                        <div class="mdl-card__title mdl-card--expand"></div>
                                        <div class="mdl-card__actions">
                                            <span class="demo-card-image__filename">Le falcon 9</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mdl-cell mdl-cell--4-col">
                                    <div class="demo-card-image mdl-card deux mdl-shadow--2dp">
                                        <div class="mdl-card__title mdl-card--expand"></div>
                                        <div class="mdl-card__actions">
                                            <span class="demo-card-image__filename">Décolage du falcon 9</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="mdl-cell mdl-cell--4-col">
                                    <div class="demo-card-image mdl-card trois mdl-shadow--2dp">
                                        <div class="mdl-card__title mdl-card--expand"></div>
                                        <div class="mdl-card__actions">
                                            <span class="demo-card-image__filename">Vu du moteur 2 de la fusée</span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <h3>Quelle est la mission ?</h3>
                            <p>
                                Le but de cette mission, nommée Iridium Next 41-50, est, comme son nom le laisse entendre, de mettre en orbite pas moins
                                de dix satellites de télécommunications. Il s’agit plus précisément d’une grappe de satellites
                                de nouvelle génération — d’où le nom de Next — et dont le poids et les dimensions par engin
                                sont relativement contenus, ce qui permet d’en emporter un grand nombre lors d’un vol.
                            </p>

                            <p>
                                C’est SpaceX qui a remporté le contrat. L’entreprise américaine opérera un lanceur Falcon 9, dont le premier étage a déjà
                                servi lors d’un précédent vol. La réutilisation du matériel qui a déjà été éprouvé auparavant
                                est l’une des marques de fabrique de la société, qui cherche à vendre des missions à bas
                                coût, en trouvant des marges d’économie. La grappe sera placée sur une orbite polaire, à
                                une altitude de 626 km.
                            </p>
                            <p>
                                Il s’agit du cinquième lancement
                                <a href="https://www.iridium.com/network/iridium-next/">Iridium Next</a> . Avec ce réseau, il s’agit de proposer une capacité de bande passante plus
                                importante et des débits plus élevés, mais aussi offrir des moyens additionnels pour suivre
                                en temps réel les avions en vol afin de ne plus assister à un incident comme le vol MH370,
                                qui a disparu quelque part au-dessus de l’océan et dont la zone de crash n’a jamais été déterminée.
                            </p>
                            <h3>Quand le décollage a lieu ?</h3>
                            <p>
                                Le lancement était prévu pour le jeudi 29 mars, depuis la Californie, plus précisément à partir de la base militaire américaine
                                de Vanderberg, qui est administrée par l’armée de l’air. Cependant, SpaceX a pris la décision
                                de reporter le grand jour au vendredi 30 mars, à 7h13 du matin heure locale.
                            </p>
                            <p>
                                En France métropolitaine, le décollage pourra être suivi à partir de 16h13.
                            </p>
                            <p>
                                Une mise à feu statique du lanceur Falcon 9 a eu lieu le 25 mars pour s’assurer que tout est en ordre. L’opération s’est
                                bien déroulée.
                            </p>
                            <h3>Comment suivre le lancement en direct</h3>
                            <p>
                                Le lancement sera normalement retransmis directement sur le site officiel de SpaceX mais aussi sur sa chaîne YouTube.
                            </p>
                            <div class="demo-crumbs mdl-color-text--grey-500">
                                <p>
                                    Source :
                                    <a target="_blank" href="https://www.numerama.com/sciences/338673-spacex-comment-suivre-la-mise-en-orbite-de-dix-satellites-dun-coup-avec-un-lanceur-falcon-9.html">Numerama</a>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
            </main>
            <footer class="mdl-mini-footer">
                <div class="mdl-mini-footer__left-section">
                    <div class="mdl-logo">Site crée par</div>
                    <ul class="mdl-mini-footer__link-list">
                        <li>
                            <a href="">Florian287001</a>
                        </li>
                    </ul>
                </div>
                <div class="mdl-mini-footer__right-section">
                    <div class="mdl-logo">FrameWork utilisé :</div>
                    <ul class="mdl-mini-footer__link-list">
                        <li>
                            <a href="https://getmdl.io/" target="_blank">Material Design Lite</a>
                        </li>
                    </ul>
                </div>
            </footer>
        </div>
    </div>

    <script>
        particlesJS.load('particles-js', 'particles.json',
            function () {
                console.log('chargement de particles.json...')
            })
    </script>

</body>


</html>