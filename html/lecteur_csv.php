<?php

session_start();

if(!isset($_SESSION["ID"])){
session_unset();
session_destroy();

header('Location: index.html'); // Renvoie Vers la page index.php

exit(); // Fin de l'éxecution du PHP

}

?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style_xvg.css" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.blue_grey-pink.min.css">
    <script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
    <script src="../lib/particles.js-master/particles.min.js"></script>
    <script src="../lib/particles.js-master/app.js"></script>
    <title>Presentation : Verge (xvg)</title>
</head>

<body>
    <div id="particles-js">
        <!-- Always shows a header, even in smaller screens. -->
        <div class="mdl-layout mdl-layout mdl-js-layout mdl-layout--fixed-header bg">
            <header class="mdl-layout__header">
                <div class="mdl-layout__header-row">
                    <!-- Title -->
                    <span class="mdl-layout-title">Presentation : Verge (xvg)</span>
                    <!-- Add spacer, to align navigation to the right -->
                    <div class="mdl-layout-spacer"></div>
                    <!-- Navigation. We hide it in small screens. -->
                    <nav class="mdl-navigation mdl-layout--large-screen-only">
                        <a class="mdl-navigation__link" href="./ecommerce/acceuil.html">E-Commerce</a>
                    </nav>
                </div>
            </header>



            <div class="mdl-layout__drawer mdl-color--blue-grey-900 mdl-color-text--blue-grey-50" aria-hidden="true" style="display: flex">

                <header class="demo-drawer-header">
                    <span class="mdl-chip__contact_contact mdl-color--teal mdl-color-text--white">Admin</span>
                    <div class="demo-avatar-dropdown">
                        <span>Administrteur</span>
                        <div class="mdl-layout-spacer"></div>
                    </div>
                </header>

                <nav class="demo-navigation mdl-navigation mdl-color--blue-grey-800">
                    <a class="mdl-navigation__link" href="">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">home</i>Page d'accueil</a>
                    <a class="mdl-navigation__link" href="./article.php">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">description</i>Article</a>
                    <a class="mdl-navigation__link" href="./Presentation_xvg.php">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">description</i>Présentation XVG</a>
                    <a class="mdl-navigation__link" href="./phishing_Cdiscount.html">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">priority_high</i>Phishing Cdiscount</a>
                    <a class="mdl-navigation__link" href="./ecommerce/acceuil.html">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">shopping_basket</i>E-commerce</a>
                    <div class="mdl-layout-spacer"></div>
                    <a class="mdl-navigation__link" href="../php/deconnection.php">
                        <i class="mdl-color-text--blue-grey-400 material-icons" role="presentation">power_settings_new</i>Déconnection</a>
                </nav>
            </div>

            <main class="mdl-layout__content" style="height: 90%;">
                <div class="page-content">
                    <!-- Your content goes here -->
                    <center id="center" class="centre_logging">

                        

                            <?php

echo("<table class=\"mdl-data-table mdl-js-data-table mdl-data-table--selectable mdl-shadow--2dp\">");

            $row = 1;
         if (($handle = fopen("../example.csv", "r")) !== false){
        while (($data = fgetcsv($handle, 1000, ";")) !== false){

            $num = count($data);
            $row++;
            echo("<body>");
            echo("<tr>");
            for ($c=0; $c < $num; $c++){
                //echo $data[$c] . " | ";

                echo("<td class=\"mdl-data-table__cell--non-numeric\">".$data[$c]."</td>");
            }

            echo("</tr>");
            echo("</body>");
        }
        fclose($handle);
    }

    echo("</table>");

    ?>

                         
                    </center>
                </div>
            </main>
            <footer class="mdl-mini-footer">
                <div class="mdl-mini-footer__left-section">
                    <div class="mdl-logo">Site crée par</div>
                    <ul class="mdl-mini-footer__link-list">
                        <li>
                            <a href="">Florian287001</a>
                        </li>
                    </ul>
                </div>
                <div class="mdl-mini-footer__right-section">
                    <div class="mdl-logo">FrameWork utilisé :</div>
                    <ul class="mdl-mini-footer__link-list">
                        <li>
                            <a href="https://getmdl.io/" target="_blank">Material Design Lite</a>
                        </li>
                    </ul>
                </div>
            </footer>
        </div>
    </div>

    <script>
        particlesJS.load('particles-js', 'particles.json',
            function () {
                console.log('chargement de particles.json...')
            })
    </script>

</body>


</html>